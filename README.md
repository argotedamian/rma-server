# Instalacion
1. Copiar el repositorio
2. npm install
3. Crear tus variables de entorno (.env)

```
PORT=4000
DATABASE=serviciotecnico
USER=root
PASS=
HOST=localhost
SECRETO=palabrasecreta
```

## Requsitos
* Nodejs, npm
* MySQL
* Crear una base de datos llamada **serviciotecnico**

## Run
```
npm start
```
Las tablas se crearan automaticamente en la base de datos

## Endpoints
Primero se deben crear los roles **admin**,**Supervisor** y **Tecnico**
```
POST - http://localhost:4000/api/roles

GET - http://localhost:4000/api/roles

Ejemplo:
        {
            "nombre":"Supervisor",
            "descripcion":"agrega repuestos,administra reparaciones"
        }
```

Luego crear **usuarios**
```
POST - http://localhost:4000/api/usuarios

Ejemplo: 
    {
        "nombre":"tecnico1",
        "password":"tecnico",
        "roleId":2
    }
```
### Como usar la API
Autenticacion
```
POST - http://localhost:4000/api/autenticacion - Login usuarios

GET - http://localhost:4000/api/autenticacion - Obtener usuario en el request
```
Equipos
```
POST - http://localhost:4000/api/equipo
Ejemplo:
    {
        "marca":"Equipo Demo",
        "modelo": "Modelo Demo"
    }
```

Clientes
```
POST - http://localhost:4000/api/cliente
GET - http://localhost:4000/api/cliente
GET - http://localhost:4000/api/cliente/:id
PATCH - http://localhost:4000/api/cliente/:id

Ejemplo:
    {
        "nombre":"CLIENTE DEMO SA",
        "email":"demo@idgroup.com",
        "telefono":"123456",
        "direccion":"Barracas 123"
    }

```

Reparaciones
```
POST - http://localhost:4000/api/reparacion
GET - http://localhost:4000/api/reparacion
GET - http://localhost:4000/api/reparacion/:id
PATCH - http://localhost:4000/api/reparacion/:id
Ejemplo:
    {
        "numeroserie":"numero de serie demo",
        "falla":"falla demo",
        "observaciones":"demo de observaciones....",
        "imagen":"imagendemo.jpg",
        "clienteId":5,
        "equipoId":1

    }
```

Repuestos
```
POST - http://localhost:4000/api/repuesto
GET - http://localhost:4000/api/repuesto
Ejemplo:
    {
        "sku":"sku 1234 Demo 4",
        "precio":7.99,
        "stock":1
    }
```
Presupuestos
```
POST - http://localhost:4000/api/presupuesto
PATCH - http://localhost:4000/api/presupuesto/:id
Ejemplo:
    {
        "diagnostico":"diagnostico demo de presupuesto",
        "reparacioneId":1
    }
```
Items
```
POST - http://localhost:4000/api/item
DELETE - http://localhost:4000/api/item/:id
Ejemplo:
    {
        "descripcion":"sku 1234567 Demo 3",
        "cantidad":2,
        "reparacioneId":1
    }
```



