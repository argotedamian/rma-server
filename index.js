const express = require('express');
const db = require('./config/db');


const rolRouter = require('./routes/rol');
const usuarioRouter = require('./routes/usuario');
const autenticacionRouter = require('./routes/autenticacion');
const reparacionRouter = require('./routes/reparacion');
const clienteRouter = require('./routes/cliente');
const equipoRouter = require('./routes/equipo');
const repuestoRouter = require('./routes/repuesto');
const itemRouter = require('./routes/item');
const presupuestoRouter = require('./routes/presupuesto');


require('dotenv').config();
const PORT = process.env.PORT || 4000;

//test de conexion
const testConexion = async () =>{
    try {
        await db.authenticate();
        console.log('DB conectado');
    } catch (error) {
        console.error('Falla de conexion a la base de datos',error);
    }
}
testConexion();

//crear tablas
require('./models/Rol');
require('./models/Usuario');
require('./models/Reparacion');
require('./models/Cliente');
require('./models/Equipo');
require('./models/Repuesto');
require('./models/Item');
require('./models/Presupuesto');
const crearTablas = async () =>{
    try {
        await db.sync();
        console.log('Tablas creadas');
    } catch (error) {
        console.error('Error al crear las tablas',error);
    }
}
crearTablas();

const app = express();


//habilitar express.json
app.use(express.json());

//rutas
app.use('/api/roles',rolRouter);
app.use('/api/usuarios',usuarioRouter);
app.use('/api/autenticacion',autenticacionRouter);
app.use('/api/reparacion',reparacionRouter);
app.use('/api/cliente',clienteRouter);
app.use('/api/equipo',equipoRouter);
app.use('/api/repuesto',repuestoRouter);
app.use('/api/item',itemRouter);
app.use('/api/presupuesto',presupuestoRouter);


app.listen(PORT,()=>{
    console.log(`Servidor funcionando en ${PORT}`);
})