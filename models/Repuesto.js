const db = require('../config/db');
const {DataTypes} = require('sequelize');
const Reparacion = require('./Reparacion');


const Repuesto = db.define('repuestos',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    sku:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    precio:{
        type:DataTypes.DOUBLE,
        allowNull:false
    },
    stock:{
        type:DataTypes.INTEGER(2),
        defaultValue:0
    }
});

module.exports=Repuesto;