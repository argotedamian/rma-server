const db = require('../config/db');
const {DataTypes} = require('sequelize');

const Cliente = require('./Cliente');
const Equipo = require('./Equipo');

const Reparacion = db.define('reparaciones',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    numeroserie:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    falla:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    observaciones:{
        type:DataTypes.TEXT
    },
    imagen:{
        type:DataTypes.STRING(20),
        allowNull:false
    }
});

Reparacion.belongsTo(Cliente);
Reparacion.belongsTo(Equipo);

module.exports=Reparacion;