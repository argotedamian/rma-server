const db = require('../config/db');
const {DataTypes} = require('sequelize');

const Rol = require('./Rol');

const Usuario = db.define('usuarios',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    nombre:{
        type:DataTypes.STRING(80),
        allowNull:false
    },
    password:{
        type:DataTypes.STRING(80),
        allowNull:false
    }
});

Usuario.belongsTo(Rol);

module.exports=Usuario;
