const db = require('../config/db');
const {DataTypes} = require('sequelize');

const Reparacion = require('./Reparacion');
const Item = require('./Item');

const Presupuesto = db.define('presupuestos',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    diagnostico:{
        type:DataTypes.TEXT,
        allowNull:false
    },
    estado:{
        type:DataTypes.BOOLEAN,
        defaultValue:null
    }
});

Presupuesto.belongsTo(Reparacion);
Presupuesto.hasMany(Item);


module.exports = Presupuesto;