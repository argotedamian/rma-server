const db = require('../config/db');
const {DataTypes} = require('sequelize');

const Equipo = db.define('equipos',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    marca:{
        type:DataTypes.STRING(30),
        allowNull:false
    },
    modelo:{
        type:DataTypes.STRING(30),
        allowNull:false
    }
});


module.exports=Equipo;