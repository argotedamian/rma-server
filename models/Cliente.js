const db = require('../config/db');
const {DataTypes} = require('sequelize');

const Cliente = db.define('clientes',{

    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    nombre:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    email:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    telefono:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    direccion:{
        type:DataTypes.STRING(100),
        allowNull:false
    }
});

module.exports=Cliente;