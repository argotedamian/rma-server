const db = require('../config/db');
const {DataTypes} = require('sequelize');


const Rol = db.define('roles',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    nombre:{
        type:DataTypes.STRING(20),
        allowNull:false
    },
    descripcion:{
        type:DataTypes.STRING(80),
        allowNull:false
    }
    
});

module.exports=Rol;