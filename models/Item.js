const db = require('../config/db');
const {DataTypes} = require('sequelize');

const Reparacion = require('./Reparacion');

const Item = db.define('items',{
    id:{
        type: DataTypes.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
        
    },
    descripcion:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    cantidad:{
        type:DataTypes.INTEGER(2),
        defaultValue:1
    }

});

Item.belongsTo(Reparacion);

module.exports=Item;