const Usuario = require('../models/Usuario');

exports.crear_usuario = async (req,res) =>{
    const {nombre,password,roleId} =req.body;

    const usuario = await Usuario.create({nombre,password,roleId});
    res.status(201).json({usuario});
}