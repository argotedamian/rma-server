const Rol = require('../models/Rol');

exports.crear_rol = async (req,res) =>{

    const {nombre,descripcion} = req.body;
    console.log(req.body)
    const rol = await Rol.create({nombre,descripcion});
    res.status(201).json({rol});

}

exports.lista_roles = async (req,res) =>{

    const roles = await Rol.findAll();
    res.status(200).json({roles});
}