const Repuesto = require('../models/Repuesto');


exports.crear_repuesto = async (req,res) =>{
    const {sku,precio,stock} =req.body;

    if(req.usuario.rol === 'Tecnico') return res.status(403).json({msg:'Permisos no valido'});

    const repuesto = await Repuesto.create({sku,precio,stock});
    res.status(201).json({repuesto});
}

exports.repuestos_list = async (req,res) =>{
    const repuestos = await Repuesto.findAll();

    res.status(200).json({repuestos});
}

exports.editar_repuesto= async (req,res) =>{
    try {

        if(req.usuario.rol !== 'admin') return res.status(403).json({msg:'Permiso no valido'});

        const repuesto = await Repuesto.findByPk(req.params.id);
        
        if(!repuesto) return res.status(404).json({msg:'Repuesto no encontrado'});

        await Repuesto.update(req.body,{
            where:{
                id:repuesto.id
            }
        });

        const repuestoActualizado = await Repuesto.findByPk(req.params.id);

        res.status(200).json({repuestoActualizado});
        
    } catch (error) {
        console.log(error);
        res.status(500).json({msg:'Hubo un error'});
    }
}

