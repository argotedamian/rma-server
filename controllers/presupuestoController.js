const Presupuesto = require("../models/Presupuesto");
const Item = require("../models/Item");
const Repuesto = require("../models/Repuesto");

exports.crear_presupuesto = async (req, res) => {
  try {
    const { diagnostico, reparacioneId } = req.body;

    //creo el presupuesto
    const presupuesto = await Presupuesto.create({
      diagnostico,
      reparacioneId,
    });

    //actualizo todos los items de la reparaioneId
    await Item.update(
      { presupuestoId: presupuesto.id },
      {
        where: {
          reparacioneId: presupuesto.reparacioneId,
        },
      }
    );

    res.status(201).json({ presupuesto });
  } catch (error) {
    console.log(error);
    res.status(500).json({ msg: "Hubo un error" });
  }
};

exports.editar_presupuesto = async (req, res) => {
  try {
    //verificar usuario
    if (req.usuario.rol !== "Supervisor")
      return res.status(403).json({ msg: "Permiso no valido" });
    //obtener presupuesto
    const presupuesto = await Presupuesto.findByPk(req.params.id);
    if (!presupuesto) return res.status(404).json({ msg: "No encontrado" });

    const { estado } = req.body;

    await Presupuesto.update(req.body, {
      where: {
        id: presupuesto.id,
      },
    });

    if (!estado) return res.status(200).json({ msg: "Presupuesto Rechazado" });

    //obtener items del presupuesto
    const items = await Item.findAll({
      where: {
        presupuestoId: presupuesto.id,
      },
    });

    //actualizar el stock
    actualizarStock(items);

    const presupuestoActualizado = await Presupuesto.findByPk(req.params.id);
    res.status(200).json({ presupuestoActualizado });
  } catch (error) {
    console.log(error);
    res.status(500).json({ msg: "Hubo un error" });
  }
};

const actualizarStock = async (listaItems) => {
  //recorrer el array
  listaItems.forEach(async (item) => {
    //busco el repuesto
    let repuesto = await Repuesto.findOne({ where: { sku: item.descripcion } });

    let resta = repuesto.stock - item.cantidad;
    //actualizo stock por cada item
    await Repuesto.update(
      { stock: resta },
      {
        where: {
          sku: item.descripcion,
        },
      }
    );
  });
};
