const Equipo = require('../models/Equipo');


exports.crear_equipo = async (req,res) =>{

    const {marca,modelo} = req.body;

    if(req.usuario.rol === 'Tecnico') return res.status(403).json({msg: 'Permiso no valido'});
    
    const equipo = await Equipo.create({marca,modelo});
    res.status(201).json({equipo});
}