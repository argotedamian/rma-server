const Item = require('../models/Item');

exports.crear_item = async (req,res) =>{
    
    const {descripcion,cantidad,reparacioneId} = req.body
    console.log(req.body);
    const item = await Item.create({descripcion,cantidad,reparacioneId});

    res.status(201).json({item});

}

exports.eliminar_item = async(req,res) =>{
    await Item.destroy({
        where:{
            id:req.params.id
        }
    });

    res.status(200).json({msg:'Item Eliminado'});
}