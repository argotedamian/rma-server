const Cliente = require("../models/Cliente");

exports.crear_cliente = async (req, res) => {
  const { nombre, email, telefono, direccion } = req.body;

  if (req.usuario.rol !== "admin")
  return res.status(403).json({ msg: "Permisos no validos" });

  const cliente = await Cliente.create({ nombre, email, telefono, direccion });
  res.status(201).json({ cliente });
};

exports.clientes_list = async (req, res) => {
  const clientes = await Cliente.findAll();

  res.status(200).json({ clientes });
};

exports.obtener_cliente = async (req, res) => {
  const cliente = await Cliente.findByPk(req.params.id);

  if (!cliente) return res.status(404).json({ msg: "Cliente no encontrado" });

  res.status(200).json({ cliente });
};

exports.editar_cliente = async (req, res) => {
  try {
    const cliente = await Cliente.findByPk(req.params.id);

    if (!cliente) return res.status(404).json({ msg: "Cliente no encontrado" });

    await Cliente.update(req.body, {
      where: {
        id: cliente.id,
      },
    });
    const clienteActualizado = await Cliente.findByPk(req.params.id);
    res.status(200).json({ clienteActualizado });
    
  } catch (error) {
    console.log(error);
    res.status(500).json({ msg: "Hubo un error" });
  }
};
