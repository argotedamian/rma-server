const Reparacion = require('../models/Reparacion');

exports.crear_reparacion = async (req,res) =>{

    const {numeroserie,falla,observaciones,imagen,clienteId,equipoId} =req.body;

    try {
        const reparacion = await Reparacion.create({numeroserie,falla,observaciones,imagen,clienteId,equipoId});
        res.status(201).json({reparacion});
    } catch (error) {
        console.log(error)
        res.status(400).json({msg:'Error al crear la reparacion'})
    }
    
}

exports.reparaciones_list = async (req,res) =>{
    const reparaciones = await Reparacion.findAll();

    res.status(200).json({reparaciones});
}

exports.obtener_reparacion = async (req,res) =>{
    const reparacion = await Reparacion.findByPk(req.params.id);

    if(!reparacion) return res.status(404).json({msg:'Reparacion no encontrada!'})

    res.status(200).json({reparacion});
}

exports.editar_reparacion = async (req,res) =>{
    try {
        if(!reparacion) return res.status(404).json({msg:'Reparacion no encontrada'});
        //obtener id
        const reparacion = await Reparacion.findByPk(req.params.id);

        

        //actualizar
        await Reparacion.update(req.body,{
            where:{
                id:reparacion.id
            }
        });

        const reparacionActualizada = await Reparacion.findByPk(req.params.id);

        res.status(200).json({reparacionActualizada});

    } catch (error) {
        console.log(error);
        res.status(500).json({msg:'Hubo un error'})
    }

}