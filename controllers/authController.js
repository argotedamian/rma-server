const Usuario = require('../models/Usuario');
const Rol = require('../models/Rol');
const jwt = require('jsonwebtoken');
require('dotenv').config();
exports.autenticar = async (req,res) =>{
    
    const {nombre,password} = req.body;
    //verificar si el usuario existe
    let usuario = await Usuario.findOne({
        where:{
            nombre
        },
        include:Rol
    });

    if(!usuario) return res.status(404).json({msg:'El usuario incorrecto'});
    //verificar contraseña
    if(password !== usuario.password) return res.status(404).json({msg:'Password incorrecto'});
    //crear token
    
    const token = jwt.sign({
        nombre:usuario.nombre,
        rol:usuario.role.nombre
    },process.env.SECRETO,{
        expiresIn:'2h'
    });

    res.json({token});
}


exports.usuarioAutenticado = (req,res) =>{

    res.json({usuario:req.usuario});
}