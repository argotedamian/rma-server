const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = (req,res,next) =>{

    const authHeader = req.get('Authorization');

    if(authHeader){
        const token = authHeader.split(' ')[1];

        try {
            const usuario = jwt.verify(token,process.env.SECRETO);

            req.usuario = usuario;
            
        } catch (error) {
            console.log(error);
            console.log('token no valido')
        }
    }

    return next();

}