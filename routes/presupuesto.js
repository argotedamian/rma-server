const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const presupuestoController = require('../controllers/presupuestoController');


router.post('/',
    auth,
    presupuestoController.crear_presupuesto
);


router.patch('/:id',
    auth,
    presupuestoController.editar_presupuesto
);







module.exports=router;