const express = require('express');
const router = express.Router();

const rolController = require('../controllers/rolController');


router.post('/',rolController.crear_rol);
router.get('/',rolController.lista_roles);




module.exports=router;