const express = require('express');
const router = express.Router();
const reparacionController = require('../controllers/reparacionController');
const auth = require('../middleware/auth');

router.post('/',
    auth,
    reparacionController.crear_reparacion
);

router.get('/',
    auth,
    reparacionController.reparaciones_list
);

router.get('/:id',
    auth,
    reparacionController.obtener_reparacion
);

router.patch('/:id',
    auth,
    reparacionController.editar_reparacion
);







module.exports=router;