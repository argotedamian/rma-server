const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const equipoController = require('../controllers/equipoController');

router.post('/',
    auth,
    equipoController.crear_equipo
);





module.exports=router;