const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const clienteController = require('../controllers/clienteController');

router.post('/',
    auth,
    clienteController.crear_cliente
);

router.get('/',
    auth,
    clienteController.clientes_list
);

router.get('/:id',
    auth,
    clienteController.obtener_cliente
);

/**TODO: Buscar por nombre o email*/

router.patch('/:id',
    auth,
    clienteController.editar_cliente
);




module.exports=router;