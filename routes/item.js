const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const itemController = require('../controllers/itemController');

router.post('/',
    auth,
    itemController.crear_item
);

router.delete('/:id',
    auth,
    itemController.eliminar_item
);



module.exports=router;