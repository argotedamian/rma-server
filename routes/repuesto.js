const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const repuestoController = require('../controllers/repuestoController');

router.post('/',
    auth,
    repuestoController.crear_repuesto
);

router.get('/',repuestoController.repuestos_list);

router.patch('/:id',
    auth,
    repuestoController.editar_repuesto
);




module.exports=router;